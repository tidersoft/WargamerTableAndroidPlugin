package httpdplugin.tidersoft.org.library.com.wargamer.server;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/*
 * #%L
 * NanoHttpd-Websocket
 * %%
 * Copyright (C) 2012 - 2015 nanohttpd
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the nanohttpd nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Logger;

import httpdplugin.tidersoft.org.library.org.nanohttpd.InternalRewrite;
import httpdplugin.tidersoft.org.library.org.nanohttpd.WSServer;
import httpdplugin.tidersoft.org.library.org.nanohttpd.WebServerPlugin;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Player;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;


/**
 * @author Paul S. Hawke (paul.hawke@gmail.com) On: 4/23/14 at 10:31 PM
 */
public class WargamerServer  {

    /**
     * logger to log to.
     */
    private static final Logger LOG = Logger.getLogger(WargamerServer.class.getName());

    private final boolean debug;
    private AsyncHttpServer server ;
    private AsyncServer mAsyncServer;


   // public HashMap<String,HTTPSession> sesje;

    public WSServer wsServer;
    
    
    public WargamerServer(int port, boolean debug) {

        this.debug = debug;
        server = new AsyncHttpServer();
        mAsyncServer = new AsyncServer();
       // sesje = new HashMap<String,HTTPSession>();
       }


    private void addFile(File dir, String reg){
        System.out.println(dir.getAbsolutePath());
        for(int i =0;i <dir.listFiles().length;i++){
            final File f=dir.listFiles()[i];
            if(f.isFile()){
                final int finalI = i;
                System.out.println(f.getAbsolutePath());
                String regis = reg+"/"+f.getName();
                System.out.println(regis);
                server.get(regis, new HttpServerRequestCallback() {
                    @Override
                    public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                        //     System.out.println(request.getPath());
                        //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                        //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                        String login="";



                        response.code(200);
                        if(request.getHeaders().get("Cookie")!=null)
                            login = request.getHeaders().get("Cookie");

                        System.out.println(login);
                        response.sendFile(f);
                        response.end();
                    }
                });

            }else if(f.isDirectory()){
                addFile(f,reg+"/"+f.getName());
            }

        }
    }

    public HashMap<String,String> sesje;


    public void setSession(AsyncHttpServerResponse response,String login){

        String id=Engine.instance().RandomID();
        response.getHeaders().add("Set-Cookie",id);
        sesje.put(id,login);

    }

    public void startServer(final int port) {

        server.get("/", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                System.out.println(request.getPath());
                //     Iterator<NameValuePair> it=request.getQuery().iterator();
                //   response.setContentType(MIME_TYPES.get("html"));
                String uri="";

                String login="";


                if(request.getHeaders().get("Cookie")!=null)

                    login = request.getHeaders().get("Cookie");

                System.out.println(login);
                //if(uri.equals("/")){
                if(sesje.containsKey(login)){
                    uri="/index.html";
                }else{
                    uri="/register.html";

                }




                // }
                response.code(200);
                response.sendFile(new File(Engine.instance().getPath()+uri));
                response.end();
            }
        });

        System.out.println(Engine.instance().getPath());

        addFile(new File(Engine.instance().getPath()),"");
        //   server.directory("/",new File(Engine.instance().getPath()+"/index.html"));


        server.post("/", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                if(request.getHeaders().get("Cookie")==null){
                    if(request.getHeaders().getMultiMap().getString("login")!=null){
                        setSession(response,request.getHeaders().getMultiMap().getString("login"));}
                }
                String uri="/index.html";

                response.code(200);
                response.sendFile(new File(Engine.instance().getPath()+uri));
                response.end();
            }
        });
        setRespons();
                  server.listen(mAsyncServer, port);
        //  webView.loadUrl("http://localhost:6060");
        System.out.println("HTTPD Server start at port: "+port);

    }

    public void setRespons(){





        server.get("/GetPlayers", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";

                String msg="";

                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");


                msg+= Engine.instance().getPlayers();


                response.code(200);

                System.out.println(login);
                response.send(MIME_TYPES.get("json"),msg);
                response.end();
            }
        });  server.get("/GetMe", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";

                String msg="";


                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");

                //   engine.CreatePlayer();
                if(Engine.instance().players.containsKey(login)){
                    Player pl=Engine.instance().players.get(login);
                    msg+="{\"Name\":\""+pl.name+"\",\"Icon\":\""+pl.icona+"\",\"Lang\":\""+pl.lang+"\"}";
                }


                response.code(200);

                System.out.println(login);
                response.send(MIME_TYPES.get("json"),msg);
                response.end();
            }
        });

        server.get("/GetOuters", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";

                String msg="";

                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");


                msg+= Engine.instance().getOuterPlayers(login);




                response.code(200);
                System.out.println(login);
                response.send(MIME_TYPES.get("json"),msg);
                response.end();
            }
        });


        server.get("/GetIp", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";

                String msg="";

                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");


                msg+=Engine.instance().getIpList();




                response.code(200);
                System.out.println(login);
                response.send(MIME_TYPES.get("json"),msg);
                response.end();
            }
        });

        server.get("/GetSystems", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";

                String msg="";

                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");


                msg+=Engine.instance().getSystems();




                response.code(200);
                System.out.println(login);
                response.send(MIME_TYPES.get("json"),msg);
                response.end();
            }
        });
        server.get("/logout", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                //     System.out.println(request.getPath());
                //      System.out.println(MIME_TYPES.get(request.getPath().split(".")[1]));
                //     response.setContentType(MIME_TYPES.get(request.getPath().split(".")[1]));
                String login="";


                Engine.instance().Logout(login);


                sesje.remove(login);

                login=Engine.instance().RandomID();

                //   engine.CreatePlayer();
                String uri="/index.html";
                if(sesje.containsKey(login)){
                    uri="/index.html";
                }else{
                    uri="/register.html";

                }
                if(request.getHeaders().get("Cookie")!=null)
                    login = request.getHeaders().get("Cookie");
                response.code(200);

                System.out.println(login);
                response.sendFile(new File(Engine.instance().getPath()+uri));
                response.end();
            }
        });



    }
    
    public String RandomID(){
    	String text="";
    	
    	Random r=new Random();
    	for(int i=0;i<32;i++){
    		text += getchar(r.nextInt(60));
    		
    	}
    	
    	return text;
    	
    	
    }
    
    
  public String getchar(int i){
    	
    	if(i==0)return "0";
    	if(i==1)return "1";
    	if(i==2)return "2";
    	if(i==3)return "3";
    	if(i==4)return "4";
    	if(i==5)return "5";
    	if(i==6)return "6";
    	if(i==7)return "7";
    	if(i==8)return "8";
    	if(i==9)return "9";
    	
    	if(i==10)return "a";
    	if(i==11)return "b";
    	if(i==12)return "c";
    	if(i==13)return "d";
    	if(i==14)return "e";
    	if(i==15)return "f";
    	if(i==16)return "g";
    	if(i==17)return "h";
    	if(i==18)return "i";
    	if(i==19)return "j";
    	if(i==20)return "k";
    	if(i==21)return "l";
    	if(i==22)return "m";
    	if(i==23)return "n";
    	if(i==24)return "o";
    	if(i==25)return "p";
    	if(i==26)return "r";
    	if(i==27)return "s";
    	if(i==28)return "t";
    	if(i==29)return "u";
    	if(i==30)return "w";
    	if(i==31)return "q";
    	if(i==32)return "x";
    	if(i==33)return "y";
    	if(i==34)return "z";
    	
    	if(i==35)return "A";
    	if(i==36)return "B";
    	if(i==37)return "C";
    	if(i==38)return "D";
    	if(i==39)return "E";
    	if(i==40)return "F";
    	if(i==41)return "G";
    	if(i==42)return "H";
    	if(i==43)return "I";
    	if(i==44)return "J";
    	if(i==45)return "K";
    	if(i==46)return "L";
    	if(i==47)return "M";
    	if(i==48)return "N";
    	if(i==49)return "O";
    	if(i==50)return "P";
    	if(i==51)return "R";
    	if(i==52)return "S";
    	if(i==53)return "T";
    	if(i==54)return "U";
    	if(i==55)return "W";
    	if(i==56)return "Q";
    	if(i==57)return "X";
    	if(i==58)return "Y";
    	if(i==59)return "Z";
    	
    	
    	
    	
    	return " ";
    }
    

/*
    @Override
    public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();
        if(method.equals(Method.POST)){
        try {
			session.parseBody(new HashMap<String, String>());
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ResponseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}}
        
        System.out.println( session.getMethod() + " " + session.getParms() );
        System.out.println(method + " '" + uri + "' ");
      //  LOG.info(method + " '" + uri + "' ");

        String msg = "";
        Map<String, String> parms = session.getParms();
        
        String login=session.getCookies().read("Wargamer");
        
        if(login==null || login=="")
            login=this.RandomID();
    
        
        
       
        
       if(session.getMethod().equals(Method.POST)){
      	  try {
      	        Map<String, String> files = new HashMap<String, String>();
      	        try {
						session.parseBody(files);
					} catch (ResponseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

      	        
      	        Set<String> keys = files.keySet();
      	        for(String key: keys){
      	            String name = key;
      	            System.out.println(name);
      	            String loaction = files.get(key);
      	            
      	            File tempfile = new File(loaction);
      	          Files.copy(tempfile.toPath(), new File(Path+File.separator+session.getParms().get("path")+File.separator+session.getParms().get("name")).toPath(), StandardCopyOption.REPLACE_EXISTING);
      	        }

      	    } catch (IOException  e) {
      	        System.out.println("i am error file upload post ");
      	        e.printStackTrace();
      	    }

       
       }

        
      
        if(uri.contentEquals("/logout")){
      	  session.getCookies().delete("Wargamer");
      	
              Engine.instance().Logout(login);
              
              
      	   sesje.remove(login);
      	   uri="/";
            login=this.RandomID();

            //   engine.CreatePlayer();
         }
        
      
        


        if(uri.equals("/PostMessage")){
          Engine.instance().PostMessage(parms);
        }

        if(uri.equals("/PutOnServer")){
            if(parms.containsKey("database") && parms.containsKey("name") && parms.containsKey("value"))
            Engine.instance().Put_On_Server(parms.get("database"),parms.get("name"),parms.get("value"));

        }

        if(uri.equals("/UpdateOnServer")){
            if(parms.containsKey("database") && parms.containsKey("id") && parms.containsKey("value"))

                Engine.instance().Update_On_Server(parms.get("database"),Integer.parseInt(parms.get("id")),parms.get("value"));

        }
        if(uri.equals("/DeleteOnServer")){
            if(parms.containsKey("database") && parms.containsKey("id") )

                Engine.instance().Delete_On_Server(parms.get("database"),Integer.parseInt(parms.get("id")));

        }

        if(uri.equals("/GetValueById")){
            if(parms.containsKey("database") && parms.containsKey("id") )

                msg+=Engine.instance().Get_Value_By_Id(parms.get("database"),Integer.parseInt(parms.get("id")));
        }

        if(uri.equals("/GetValueByName")){
            if(parms.containsKey("database") && parms.containsKey("name") )

                msg+=Engine.instance().Get_Value_By_Name(parms.get("database"),parms.get("name"));
        }



        if(uri.equals("/GetDataBase")){
            if(parms.containsKey("database"))

                msg+=Engine.instance().Get_DataBase(parms.get("database"));
        }

        if(uri.equals("/SendToAll")){
            if(parms.containsKey("message")) {
                wsServer.sendToAll(parms.get("message"));
            }
        }

        if(uri.equals("/GetBoards")){
            if(parms.containsKey("system")){
                msg+=Engine.instance().getBoards(parms.get("system"));
            }

        }

     //   if(uri.equals("/GetModule")){
      //  	if(parms.containsKey("system")&&parms.containsKey("modul")){
      //  		uri="/dane/Systemy/"+parms.get("system")+"/Module/"+parms.get("modul");
      // /
      //  	}



      //  }


        if(uri.equals("/GetSystems")){

            msg+=Engine.instance().getSystems();
        }

        if(uri.equals("/CreateGame")){
            if(parms.containsKey("name")){
                Engine.instance().Create_Game(parms.get("name"),login,parms.get("system"));

            }

        }

        if(uri.equals("/GetGame")){
            msg+=Engine.instance().getGame(login);

        }
        
        if(uri.equals("/GetFiles")){

            if(parms.containsKey("path"))
        	msg+=Engine.instance().getFiles(parms.get("path"));
        	
        }

        if(uri.equals("/GetMe")){
            if(Engine.instance().players.containsKey(login)){
                Player pl=Engine.instance().players.get(login);
                msg+="{\"Name\":\""+pl.name+"\",\"Icon\":\""+pl.icona+"\",\"Lang\":\""+pl.lang+"\"}";
            }

        }

        if(uri.equals("/GetPlayers")){
            msg+= Engine.instance().getPlayers();

        }
        if(uri.equals("/GetOuters")){
           msg+= Engine.instance().getOuterPlayers(login);

        }
        
        System.out.println("Login: "+login);
        
        if(uri.equals("/")){
        	if(sesje.containsKey(login)){
        		uri="/index.html";
        	}else{
        		uri="/register.html";
        		
        	}
        	
        	
        	
      	   
        }
        
        
        
        
  File te= new File(Engine.getPath()+uri);
        
        
        if(te.isFile()){
       	 String mimeTypeForFile = getMimeTypeForFile(uri);
            
       	 Response resp= serveFile(uri,session.getHeaders(),te,mimeTypeForFile);
       	 
       	   resp.addHeader("Access-Control-Allow-Origin", "*");
       	    	 
       return resp;
       }
        
        
        Response resp=newFixedLengthResponse(msg);
        SimpleDateFormat gmtFrmt = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        gmtFrmt.setTimeZone(TimeZone.getTimeZone("GMT"));

        	resp.addHeader("Last-modified", gmtFrmt.format(new Date()));
        	resp.addHeader("Expires",gmtFrmt.format(new Date()) );
            
        	String ip = "*";
		///	try {
		//		ip = "http://"+Inet4Address.getLocalHost().getHostAddress()+":8080";
			
        	
        	resp.addHeader("Access-Control-Allow-Origin",ip );
		//	} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
           return resp;
      
    }
    
   
    
 public static  String MIME_DEFAULT_BINARY = "application/octet-stream";

    
    /**
     * Default Index file names.
     */
   /* public static  List<String> INDEX_FILE_NAMES = new ArrayList<String>() {

        {
            add("index.html");
            add("index.htm");
        }
    };
*/

    private static Map<String, String> MIME_TYPES = new HashMap<String, String>() {

        {
            put("css", "text/css");
            put("htm", "text/html");
            put("html", "text/html");
            put("xml", "text/xml");
            put("java", "text/x-java-source, text/java");
            put("md", "text/plain");
            put("txt", "text/plain");
            put("asc", "text/plain");
            put("gif", "image/gif");
            put("jpg", "image/jpeg");
            put("jpeg", "image/jpeg");
            put("png", "image/png");
            put("mp3", "audio/mpeg");
            put("m3u", "audio/mpeg-url");
            put("mp4", "video/mp4");
            put("ogv", "video/ogg");
            put("flv", "video/x-flv");
            put("mov", "video/quicktime");
            put("swf", "application/x-shockwave-flash");
            put("js", "application/javascript");
            put("pdf", "application/pdf");
            put("doc", "application/msword");
            put("ogg", "application/x-ogg");
            put("zip", "application/octet-stream");
            put("exe", "application/octet-stream");
            put("class", "application/octet-stream");
        }
    };
 
    /*
    private boolean canServeUri(String uri, File homeDir) {
        boolean canServeUri;
        File f = new File(homeDir, uri);
        canServeUri = f.exists();
        if (!canServeUri) {
            String mimeTypeForFile = getMimeTypeForFile(uri);
            WebServerPlugin plugin = mimeTypeHandlers.get(mimeTypeForFile);
            if (plugin != null) {
                canServeUri = plugin.canServeUri(uri, homeDir);
            }
        }
        return canServeUri;
    }
    
    private static Map<String, WebServerPlugin> mimeTypeHandlers = new HashMap<String, WebServerPlugin>();


    /**
     * URL-encodes everything between "/"-characters. Encodes spaces as '%20'
     * instead of '+'.
     *//*
    private String encodeUri(String uri) {
        String newUri = "";
        StringTokenizer st = new StringTokenizer(uri, "/ ", true);
        while (st.hasMoreTokens()) {
            String tok = st.nextToken();
            if (tok.equals("/")) {
                newUri += "/";
            } else if (tok.equals(" ")) {
                newUri += "%20";
            } else {
                try {
                    newUri += URLEncoder.encode(tok, "UTF-8");
                } catch (UnsupportedEncodingException ignored) {
                }
            }
        }
        return newUri;
    }

    private String findIndexFileInDirectory(File directory) {
        for (String fileName : INDEX_FILE_NAMES) {
            File indexFile = new File(directory, fileName);
            if (indexFile.isFile()) {
                return fileName;
            }
        }
        return null;
    }

    protected Response getForbiddenResponse(String s) {
        return newFixedLengthResponse(Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: " + s);
    }

    protected Response getInternalErrorResponse(String s) {
        return newFixedLengthResponse(Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "INTERNAL ERROR: " + s);
    }

    // Get MIME type from file name extension, if possible
    public static String getMimeTypeForFile(String uri) {
        int dot = uri.lastIndexOf('.');
        String mime = null;
        if (dot >= 0) {
            mime = MIME_TYPES.get(uri.substring(dot + 1).toLowerCase());
        }
        return mime == null ? MIME_DEFAULT_BINARY : mime;
    }

    protected Response getNotFoundResponse() {
        return newFixedLengthResponse(Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Error 404, file not found.");
    }
    
    
    protected static void registerPluginForMimeType(String[] indexFiles, String mimeType, WebServerPlugin plugin, Map<String, String> commandLineOptions) {
        if (mimeType == null || plugin == null) {
            return;
        }

        if (indexFiles != null) {
            for (String filename : indexFiles) {
                int dot = filename.lastIndexOf('.');
                if (dot >= 0) {
                    String extension = filename.substring(dot + 1).toLowerCase();
                    MIME_TYPES.put(extension, mimeType);
                }
            }
            INDEX_FILE_NAMES.addAll(Arrays.asList(indexFiles));        }
       mimeTypeHandlers.put(mimeType, plugin);
        plugin.initialize(commandLineOptions);
    }

    private  boolean quiet;

    protected List<File> rootDirs;


    
    
    private Response respond(Map<String, String> headers, IHTTPSession session, String uri) {
        // Remove URL arguments
        uri = uri.trim().replace(File.separatorChar, '/');
        if (uri.indexOf('?') >= 0) {
            uri = uri.substring(0, uri.indexOf('?'));
        }

        // Prohibit getting out of current directory
        if (uri.contains("../")) {
            return getForbiddenResponse("Won't serve ../ for security reasons.");
        }

        boolean canServeUri = false;
        File homeDir = null;
        for (int i = 0; !canServeUri && i < this.rootDirs.size(); i++) {
            homeDir = this.rootDirs.get(i);
            canServeUri = canServeUri(uri, homeDir);
        }
        if (!canServeUri) {
            return getNotFoundResponse();
        }

        // Browsers get confused without '/' after the directory, send a
        // redirect.
        File f = new File(homeDir, uri);
      

      

        String mimeTypeForFile = getMimeTypeForFile(uri);
        WebServerPlugin plugin = mimeTypeHandlers.get(mimeTypeForFile);
        Response response = null;
        if (plugin != null && plugin.canServeUri(uri, homeDir)) {
            response = plugin.serveFile(uri, headers, session, f, mimeTypeForFile);
            if (response != null && response instanceof InternalRewrite) {
                InternalRewrite rewrite = (InternalRewrite) response;
                return respond(rewrite.getHeaders(), session, rewrite.getUri());
            }
        } else { 
            response = serveFile(uri, headers, f, mimeTypeForFile);
        }
        return response != null ? response : getNotFoundResponse();
    }

 

    /**
     * Serves file from homeDir and its' subdirectories (only). Uses only URI,
     * ignores all headers and HTTP parameters.
     *//*
    Response serveFile(String uri, Map<String, String> header, File file, String mime) {
        Response res;
        try {
            // Calculate etag
            String etag = Integer.toHexString((file.getAbsolutePath() + file.lastModified() + "" + file.length()).hashCode());

            // Support (simple) skipping:
            long startFrom = 0;
            long endAt = -1;
            String range = header.get("range");
            if (range != null) {
                if (range.startsWith("bytes=")) {
                    range = range.substring("bytes=".length());
                    int minus = range.indexOf('-');
                    try {
                        if (minus > 0) {
                            startFrom = Long.parseLong(range.substring(0, minus));
                            endAt = Long.parseLong(range.substring(minus + 1));
                        }
                    } catch (NumberFormatException ignored) {
                    }
                }
            }

            // get if-range header. If present, it must match etag or else we
            // should ignore the range request
            String ifRange = header.get("if-range");
            boolean headerIfRangeMissingOrMatching = (ifRange == null || etag.equals(ifRange));

            String ifNoneMatch = header.get("if-none-match");
            boolean headerIfNoneMatchPresentAndMatching = ifNoneMatch != null && (ifNoneMatch.equals("*") || ifNoneMatch.equals(etag));

            // Change return code and add Content-Range header when skipping is
            // requested
            long fileLen = file.length();

            if (headerIfRangeMissingOrMatching && range != null && startFrom >= 0 && startFrom < fileLen) {
                // range request that matches current etag
                // and the startFrom of the range is satisfiable
                if (headerIfNoneMatchPresentAndMatching) {
                    // range request that matches current etag
                    // and the startFrom of the range is satisfiable
                    // would return range from file
                    // respond with not-modified
                    res = newFixedLengthResponse(Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else {
                    if (endAt < 0) {
                        endAt = fileLen - 1;
                    }
                    long newLen = endAt - startFrom + 1;
                    if (newLen < 0) {
                        newLen = 0;
                    }

                    FileInputStream fis = new FileInputStream(file);
                    fis.skip(startFrom);

                    res = newFixedLengthResponse(Status.PARTIAL_CONTENT, mime, fis, newLen);
                    res.addHeader("Accept-Ranges", "bytes");
                    res.addHeader("Content-Length", "" + newLen);
                    res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
                    res.addHeader("ETag", etag);
                }
            } else {

                if (headerIfRangeMissingOrMatching && range != null && startFrom >= fileLen) {
                    // return the size of the file
                    // 4xx responses are not trumped by if-none-match
                    res = newFixedLengthResponse(Status.RANGE_NOT_SATISFIABLE, NanoHTTPD.MIME_PLAINTEXT, "");
                    res.addHeader("Content-Range", "bytes " + fileLen);
                    res.addHeader("ETag", etag);
                } else if (range == null && headerIfNoneMatchPresentAndMatching) {
                    // full-file-fetch request
                    // would return entire file
                    // respond with not-modified
                    res = newFixedLengthResponse(Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else if (!headerIfRangeMissingOrMatching && headerIfNoneMatchPresentAndMatching) {
                    // range request that doesn't match current etag
                    // would return entire (different) file
                    // respond with not-modified

                    res = newFixedLengthResponse(Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else {
                    // supply the file
                    res = newFixedFileResponse(file, mime);
                    res.addHeader("Content-Length", "" + fileLen);
                    res.addHeader("ETag", etag);
                }
            }
        } catch (IOException ioe) {
            res = getForbiddenResponse("Reading file failed.");
        }

        return res;
    }

  

	private Response newFixedFileResponse(File file, String mime) throws FileNotFoundException {
        Response res;
        res = newFixedLengthResponse(Status.OK, mime, new FileInputStream(file), (int) file.length());
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

	
  */
}
