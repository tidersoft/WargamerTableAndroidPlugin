package httpdplugin.tidersoft.org.library.com.wargamer.server;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;

import com.unity3d.player.UnityPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Board;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Contener;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.DataBase;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Sender_Game;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Token;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Var;
import httpdplugin.tidersoft.org.library.org.java_websocket.WebSocket;

import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Game;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Player;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Sender_Browser;


@SuppressWarnings("ALL")
public class Engine {
	   private static Engine instance;

	   
	   public HashMap<String,Player> players;
	   
	//	public ArrayList<Sender_Browser> przegladarki;
		
		public HashMap<String,Game> games;
		
        public HashMap<String,Board> boards;


    public ArrayList<String> IpAdress;

    public HashMap<String,String> IdList;
		
		public static String Path = Environment.getExternalStorageDirectory().toString()+File.separator;

        public static String path="WargamerTable";

        public static String WWW="http://wargamertable.org";

        public Context cont = UnityPlayer.currentActivity.getApplication().getBaseContext();

		public static Engine instance() {
	        if(instance == null) {
	            instance = new Engine();
	        }
	        return instance;
	    }
	   
	   public Engine(){
		   players = new HashMap<String,Player>();
		//   przegladarki = new ArrayList<Sender_Browser>();
           games =  new HashMap<String,Game>();
			boards = new HashMap<String,Board>();
           IdList = new HashMap<String,String>();
           IpAdress = new ArrayList<String>();
	   }
	   
	   public void RefreshPlayer(){
           /*
           for (HashMap.Entry<String, Player> p : players.entrySet())
               if (p.getValue().browser.conection.isOpen())
                   p.getValue().browser.conection.send("[RefreshPlayer]");*/
         }


         public String getWiFiNetwork(){
            String ssid=null;
              ConnectivityManager connManager = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);

             NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
             if (networkInfo.isConnected()) {
                 final WifiManager wifiManager = (WifiManager) cont.getSystemService(Context.WIFI_SERVICE);
                 final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                 if (connectionInfo != null && !(connectionInfo.getSSID().equals(""))) {
                     //if (connectionInfo != null && !StringUtil.isBlank(connectionInfo.getSSID())) {
                     ssid = connectionInfo.getSSID();
                 }

         }
             return ssid;

         }


    public String RandomID(){
        String text="";

        Random r=new Random();
        for(int i=0;i<32;i++){
            text += getchar(r.nextInt(60));

        }

        return text;


    }


    public String getchar(int i){

        if(i==0)return "0";
        if(i==1)return "1";
        if(i==2)return "2";
        if(i==3)return "3";
        if(i==4)return "4";
        if(i==5)return "5";
        if(i==6)return "6";
        if(i==7)return "7";
        if(i==8)return "8";
        if(i==9)return "9";

        if(i==10)return "a";
        if(i==11)return "b";
        if(i==12)return "c";
        if(i==13)return "d";
        if(i==14)return "e";
        if(i==15)return "f";
        if(i==16)return "g";
        if(i==17)return "h";
        if(i==18)return "i";
        if(i==19)return "j";
        if(i==20)return "k";
        if(i==21)return "l";
        if(i==22)return "m";
        if(i==23)return "n";
        if(i==24)return "o";
        if(i==25)return "p";
        if(i==26)return "r";
        if(i==27)return "s";
        if(i==28)return "t";
        if(i==29)return "u";
        if(i==30)return "w";
        if(i==31)return "q";
        if(i==32)return "x";
        if(i==33)return "y";
        if(i==34)return "z";

        if(i==35)return "A";
        if(i==36)return "B";
        if(i==37)return "C";
        if(i==38)return "D";
        if(i==39)return "E";
        if(i==40)return "F";
        if(i==41)return "G";
        if(i==42)return "H";
        if(i==43)return "I";
        if(i==44)return "J";
        if(i==45)return "K";
        if(i==46)return "L";
        if(i==47)return "M";
        if(i==48)return "N";
        if(i==49)return "O";
        if(i==50)return "P";
        if(i==51)return "R";
        if(i==52)return "S";
        if(i==53)return "T";
        if(i==54)return "U";
        if(i==55)return "W";
        if(i==56)return "Q";
        if(i==57)return "X";
        if(i==58)return "Y";
        if(i==59)return "Z";




        return " ";
    }


    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }




         public String getPlayerNameByWebSock(WebSocket conn){
             Iterator<Player> it =players.values().iterator();
             while(it.hasNext()){
                 Player pl=it.next();
                 if(pl.browser!=null)
                 if(pl.browser.conection.equals(conn)){
                     System.out.println("Select player "+pl.name);
                     return pl.name;

                 }

             }
             return null;

         }

         public void CreateBoard(String name, String system){
             if(!boards.containsKey(name)) {
                 Board board = new Board();
                 board.System = system;
                 board.name = name;
                 boards.put(name, board);
                 System.out.println("Creating board " + name);
             }
         }

         public void ConnectToBoard(String name,String system,WebSocket conn){
            System.out.println("Connecting to board "+name);
             if(!boards.containsKey(name)){
                CreateBoard(name,system);

             }
                 Board board = boards.get(name);
                 Sender_Game send = new Sender_Game();
                 send.conection = conn;
                 board.conection.add(send);
             System.out.println("Connected to board "+name);

         }

         public void DisconnectFromBoard(String name,WebSocket conn){
             Board board = boards.get(name);
             for(int i=0;i<board.conection.size();i++){
                 if(board.conection.get(i).conection.equals(conn)){
                     board.conection.get(i).conection.send("{\"cmd\":\"Disconect\",\"msg\":\"\"}");
                     board.conection.remove(i);
                 }

             }



         }

         public void DestroyBoard(String name){
             Board board = boards.get(name);
             for(int i=0;i<board.conection.size();i++){
                    board.conection.get(i).conection.send("{\"cmd\":\"Disconect\",\"msg\":\"\"}");
             }
             boards.remove(board.name);

         }

         public  void sendToBoard(String name,String message){
             System.out.println("Sending to board "+name);

             Board board=boards.get(name);
            for(int i=0;i<board.conection.size();i++) {
                board.conection.get(i).send(message);
                System.out.println("Send "+i+" to board "+name);

            }
             System.out.println("Sendet to board "+name);

         }



         public static String getPath(){

             return Path+path;

         }



         public static void setPath(String name){

             path=name;
         }

         public static void setWWW(String name){
             WWW=name;

         }


        public static HashMap<String, String> jsonToMap(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        return map;
      //  System.out.println("json : "+jObject);
     //   System.out.println("map : "+map);
        }






         public static void Engine_Mesg(String function,String message){
             UnityPlayer.UnitySendMessage("System_Engine", function, message);

         }

         public void Create_Game(String name,String player,String system){
             Game game = new Game(name);
             game.players.put(player,players.get(player));
             game.system=system;
             games.put(name,game);
            // String mesg="{\"Game\":\"" + name + "\",\"System\":\"" + system + "\",\"Creator\":\""+player+"\"}";
            // Engine_Mesg("Create_Game",mesg);



         }

         public void Put_On_Server(String database,String name,String value){

                DataBase db= new DataBase(cont,database);
                 db.addContener(new Contener(name,value));

         }

         public void Update_On_Server(String database,int id,String value){

                 DataBase db= new DataBase(cont,database);
                 Contener cont = db.getContener(id);
                 cont.setVarible(value);
                 db.updateContener(cont);

         }
    public void Delete_On_Server(String database,int id){

            DataBase db= new DataBase(cont,database);
            Contener cont = db.getContener(id);
            db.deleteContener(cont);

    }



    public String Get_Value_By_Id(String database,int id){
        String msg="";
            DataBase db = new DataBase(cont,database);
            Contener con = db.getContener(id);
                msg += "{\"Id\":\"" + con.getID() + "\",\"Name\":\"" + con.getName() + "\",\"Value\":\"" + con.getVarible() + "\"}";





        return msg;

    }


    private final String USER_AGENT = "Mozilla/5.0";

    // HTTP GET request
    private void sendGet(String url) throws Exception {

        //String url = "http://www.google.com/search?q=mkyong";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    // HTTP POST request
    private void sendPost(String url,String parameter) throws Exception {

     //   String url = "https://selfsolve.apple.com/wcResults.do";
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = parameter;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    public String Get_Value_By_Name(String database,String name){
        String msg="";
            DataBase db = new DataBase(cont, database);
            Contener con = db.getContenerByName(name);
            msg += "{\"Id\":\"" + con.getID() + "\",\"Name\":\"" + con.getName() + "\",\"Value\":\"" + con.getVarible() + "\"}";





        return msg;

    }






    public String getIpList(){
        String msg="{\"Ip\":[";
        ArrayList<String> con = IpAdress;
        for(int i=0;i<con.size();i++) {
            msg += "{\"Ip\":\"" + con.get(i)  + "\"}";
            if(i<con.size()-1)
                msg += ",";
        }


        msg+="]}";
        return msg;

    }
         public String Get_DataBase(String database){

             String msg="{\"Database\":[";
                DataBase db = new DataBase(cont, database);
                     ArrayList<Contener> con = (ArrayList<Contener>) db.getAllConteners();
                 for(int i=0;i<con.size();i++) {
                     msg += "{\"Id\":\"" + con.get(i).getID() + "\",\"Name\":\"" + con.get(i).getName() + "\",\"Value\":\"" + con.get(i).getVarible() + "\"}";
                     if(i<con.size()-1)
                             msg += ",";
                 }


             msg+="]}";
             return msg;
         }


	   
	   public void Logout(String login){
           players.remove(login);
         Iterator<String> it=  games.keySet().iterator();
         while(it.hasNext()){
        	 Game game=games.get(it.next());
        	 if(game.players.containsKey(login)){
        		 game.players.remove(login);
        	 }
         }
         System.out.println("Player "+login+" is logout");
	     RefreshPlayer();

        }
	   
     //   public void removeBrowser(WebSocket con){
     //   	for(int i=0;i<przegladarki.size();i++){
		
      //  		if(przegladarki.get(i).conection.equals(con)){
      //  			przegladarki.remove(i);
			
		//		}
		
       // 	}
     //   }


    public String getGame(String player){

        Iterator<Game> it= games.values().iterator();
        while(it.hasNext()){
            Game g=it.next();
            if(g.players.containsKey(player)){
                return "{\"Name\":\"" + g.name + "\",\"System\":\""+g.system+"\"}";

            }

        }

        return "";
    }

    public void PostMessage(Map<String,String> params){

        if(params.containsKey("function")){

            System.out.println("Post function "+params.get("function"));
            if(params.containsKey("object")){
                System.out.println("Post to object "+params.get("object"));

                if(params.containsKey("message")) {
                    System.out.println("Post message "+params.get("message"));

                    UnityPlayer.UnitySendMessage(params.get("object"), params.get("function"), params.get("message"));
                }else {
                    UnityPlayer.UnitySendMessage(params.get("object"), params.get("function"), "");
                }
            }else{
                System.out.println("Post to System_Engine ");

                if(params.containsKey("message")) {
                    System.out.println("Post message "+params.get("message"));

                    UnityPlayer.UnitySendMessage("System_Engine", params.get("function"), params.get("message"));
                }else {
                    UnityPlayer.UnitySendMessage("System_Engine", params.get("function"), "");
                }

            }
        }else{

            System.out.println("Not function post");
        }
    }


        public void addPlayerBrowser(String player,WebSocket con) {
            if (players.containsKey(player)){
                Sender_Browser brow = new Sender_Browser();

            brow.conection = con;
            players.get(player).browser = brow;
        }
        }

        public void addTokenToBoard(String boardname,String name,float x,float y,float z){

            Token tok=new Token();
            tok.name=name;
            tok.x=x;
            tok.y=y;
            tok.z=z;
            Board board = boards.get(boardname);
            board.tokeny.put(name,tok);

        }

        public void setVarBoard(String bname,String name,String var){
            Board board=boards.get(bname);
            if(board.vars.containsKey(name)){

                Var vare=board.vars.get(name);
                board.vars.remove(name);
                vare.value=var;
                board.vars.put(name,vare);
            }else{

                Var vare=new Var();
                vare.value=var;
                vare.name=name;
                board.vars.put(name,vare);
            }
        }

        public void setVarToken(String bname,String tname,String name,String var){
            Board board=boards.get(bname);
            if(board.tokeny.containsKey(tname)) {
                Token tok = board.tokeny.get(tname);
                if (tok.vars.containsKey(name)) {

                    Var vare =tok.vars.get(name);
                    tok.vars.remove(name);
                    vare.value = var;
                    tok.vars.put(name, vare);
                } else {

                    Var vare = new Var();
                    vare.value = var;
                    vare.name = name;
                    tok.vars.put(name, vare);
                }
            }
        }

        public void updateTokenPos(String boardn,String name,float x,float y,float z){
            Board board = boards.get(boardn);
            if(board.tokeny.containsKey(name)) {
                Token tok = board.tokeny.get(name);
                tok.x=x;
                tok.y=y;
                tok.z=z;
            }
        }

        public void addPlayerToGame(String player,String game){
            Player pl = players.get(player);
            Game gm= games.get(game);
            gm.players.put(player,pl);
        }

        public void sendBrowserMessageToPlayer(String player,String message){
            if(players.containsKey(player))
        	players.get(player).browser.send(message);
        	
        }
        
        public void sendGameMessageToPlayer(String game,String player,String message){
            if(players.containsKey(player) && games.containsKey(game))
        	games.get(game).players.get(player).browser.send(message);
        	
        }
        
        public void sendMessageToAllBrowsers(String message){
        	Iterator<Player> it=players.values().iterator();
        	while(it.hasNext()){
        		Player pl= it.next();
                pl.browser.send(message);
        		System.out.println("Send message "+message+" to player "+pl.name);
        	}
        }
        
        public void sendMessageToAllPlayerInGame(String game,String message){
        	Game g= games.get(game);
        	Iterator<String> it= g.players.keySet().iterator();
        	while(it.hasNext()){
        		 g.players.get(it.next()).browser.send(message);
        		
        	}
        }

        public void CheckPlayers(Collection<WebSocket> con){
            Iterator<Player> it =players.values().iterator();
            while(it.hasNext()) {
                Player pl = it.next();
                if (pl.browser != null)
                    if (!con.contains(pl.browser)) {
                        Logout(pl.name);

                    }

            }


        }
        
       
    	public static void Download_System(String system){
             Path = getPath();

            WebFile web;
            try {
                web = new WebFile(WWW+"/mobile/download.php?opcja=download&system="+system);
         
                File te = new File(Path+File.separator+"dane"+File.separator+"Systemy"+File.separator+system);
                te.mkdirs();
                    for(int i=0;i<web.size();i++){
                        String temp=((String)web.get(i));
                        if(temp.startsWith("file:=")){
                            String scieszka=temp.split(":=")[1];
                            new DownloadFileFromURL().execute(scieszka.replaceFirst("..", WWW),scieszka.replace("..", Path));
                            System.out.println(scieszka);
                        }
                        if(temp.startsWith("dir:=")){
                            String scieszka=temp.split(":=")[1].replace("..", Path);
                            File fi=new File(scieszka);
                            fi.mkdirs();
                            System.out.println(scieszka);
                        }
                        
				}
                       } catch (IOException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            }
		}




    public static void Download_Develop(){

        String Path = getPath();

        WebFile web;
        try {
            web = new WebFile(WWW+"/mobile/download.php?opcja=develop");

            File te = new File(Path);
            te.mkdirs();
            UnityPlayer.UnitySendMessage("System_Engine", "SetUpdateSize", ""+web.size());
            System.out.println("Web size:"+web.size());
            for(int i=0;i<web.size();i++){
                String temp=((String)web.get(i));
                if(temp.startsWith("file:=")){
                    String scieszka=temp.split(":=")[1];
                    new DownloadFileFromURL().execute(scieszka.replaceFirst("..", WWW+"/"),scieszka.replace("../dane/Test", Path));
                    System.out.println(scieszka);
                    UnityPlayer.UnitySendMessage("System_Engine", "CurrentUpdateSize", ""+i);

                }
                if(temp.startsWith("dir:=")){
                    String scieszka=temp.split(":=")[1].replace("../dane/Test", Path);
                    File fi=new File(scieszka);
                    fi.mkdirs();
                    UnityPlayer.UnitySendMessage("System_Engine", "CurrentUpdateSize", ""+i);

                    System.out.println(scieszka);
                }

            }

            UnityPlayer.UnitySendMessage("System_Engine", "DownloadDone", "");

        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void Download_Update(String version){

        String Path = getPath();

        WebFile web;
        try {
            web = new WebFile(WWW+"/mobile/download.php?opcja=update&version="+version);

            File te = new File(Path);
            te.mkdirs();
            UnityPlayer.UnitySendMessage("System_Engine", "SetUpdateSize", ""+web.size());
            System.out.println("Web size:"+web.size());

            for(int i=0;i<web.size();i++){
                String temp=((String)web.get(i));
                if(temp.startsWith("file:=")){
                    String scieszka=temp.split(":=")[1];
                    new DownloadFileFromURL().execute(scieszka.replaceFirst("..", WWW+"/"),scieszka.replace("../dane/Update"+version+"/http", Path));
                    System.out.println(scieszka);
                    UnityPlayer.UnitySendMessage("System_Engine", "CurrentUpdateSize", ""+i);

                }
                if(temp.startsWith("dir:=")){
                    String scieszka=temp.split(":=")[1].replace("../dane/Update"+version+"/http", Path);
                    File fi=new File(scieszka);
                    fi.mkdirs();
                    UnityPlayer.UnitySendMessage("System_Engine", "CurrentUpdateSize", ""+i);

                    System.out.println(scieszka);
                }

            }
            UnityPlayer.UnitySendMessage("System_Engine", "DownloadDone", "");

        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
		
		
	/*	public ArrayList<String> getFile(ArrayList<String> form,File parent,String path){
               
                    for(int i=0;i<parent.listFiles().length;i++){
				 if(parent.listFiles()[i].isFile()){
					 if((parent.listFiles()[i].getPath().endsWith(".jpg")||parent.listFiles()[i].getPath().endsWith(".png"))||(parent.listFiles()[i].getPath().endsWith(".JPG")||parent.listFiles()[i].getPath().endsWith(".PNG"))){
							
					 String temp=path+File.separator+parent.listFiles()[i].getName();
					form.add(temp);
					 }
				 }
				 if(parent.listFiles()[i].isDirectory()){
					 form=getFile(form,parent.listFiles()[i],path+File.separator+parent.listFiles()[i].getName());
					 
				 }
				 
			 }
                    
                    return form;
                }

*/




        public String getOuterPlayers(String player){
        String msg="{\"Players\":[";

            Player[] pl =  players.values().toArray(new Player[0]);

            for(int i=0;i<pl.length;i++){

                Player pltemp=pl[i];
                if(!pltemp.name.equals(player)) {
                    msg += "{\"Name\":\"" + pltemp.name + "\",\"Icon\":\"" + pltemp.icona + "\"}";
                    if(i<pl.length-1)
                        if (!pl[i+1].name.equals(player))
                            msg += ",";
                }
            }

            msg+="]}";
        return msg;
        }

    public String getBoards(String system){
        String msg="{\"Boards\":[";

        Board[] pl =  boards.values().toArray(new Board[0]);

        for(int i=0;i<pl.length;i++){

            Board pltemp=pl[i];
            if(pl[i].System.equals(system)) {
                msg += "{\"Name\":\"" + pltemp.name + "\"}";
                if (i < pl.length - 1)
                    msg += ",";
            }
        }

        msg+="]}";
        return msg;


    }

        public String getGamesName(){
            String msg="{\"Games\":[";

            Game[] pl =  games.values().toArray(new Game[0]);

            for(int i=0;i<pl.length;i++){

                Game pltemp=pl[i];
                msg += "{\"Name\":\"" + pltemp.name + "\"}";
                if(i<pl.length-1)
                    msg += ",";

            }

            msg+="]}";
            return msg;


        }

        public String getPlayersFromGame(String name){

            String msg="{\"Players\":[";

            Player[] pl =  games.get(name).players.values().toArray(new Player[0]);

            for(int i=0;i<pl.length;i++){

                Player pltemp=pl[i];
                msg += "{\"Name\":\"" + pltemp.name + "\",\"Icon\":\"" + pltemp.icona + "\"}";
                if(i<pl.length-1)
                    msg += ",";

            }

            msg+="]}";
            return msg;



        }

        public String getPlayers(){
            String msg="{\"Players\":[";

            Player[] pl =  players.values().toArray(new Player[0]);

            for(int i=0;i<pl.length;i++){

                Player pltemp=pl[i];
                    msg += "{\"Name\":\"" + pltemp.name + "\",\"Icon\":\"" + pltemp.icona + "\"}";
                    if(i<pl.length-1)
                            msg += ",";

            }

            msg+="]}";
            return msg;

        }



    public String getSystems(){
        String msg="{\"Systems\":[";

        File temp = new File(getPath()+File.separator+"Systems");

        for(int i=0;i<temp.list().length;i++){
            if(temp.listFiles()[i].isDirectory()) {
                msg += "{\"Name\":\"" + temp.list()[i] + "\"}";
                if (i < temp.list().length - 1)
                    msg += ",";
            }
        }

        msg+="]}";
        return msg;

    }

    public String getFiles(String path) {
        String msg="{\"Files\":[";

        File temp = new File(getPath()+File.separator+path);
         for(int i=0;i<temp.list().length;i++){
             if(temp.listFiles()[i].isDirectory())
                  msg+= "{\"Dir\":\""+temp.list()[i]+"\"}";
             if(temp.listFiles()[i].isFile())
                 msg+= "{\"File\":\""+temp.list()[i]+"\"}";

             if(i<temp.list().length-1)
                    msg += ",";

        }
        msg+="]}";

            return msg;

    }


}
