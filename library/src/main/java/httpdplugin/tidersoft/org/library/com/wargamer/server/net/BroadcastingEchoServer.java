package httpdplugin.tidersoft.org.library.com.wargamer.server.net;

/**
 * Created by Tiderus on 18.03.2018.
 */
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;


import java.util.logging.Level;
import java.util.logging.Logger;

import httpdplugin.tidersoft.org.library.com.wargamer.server.Engine;

public class BroadcastingEchoServer extends Thread {

    protected DatagramSocket socket = null;
    protected boolean running;
    protected byte[] buf = new byte[256];

    public BroadcastingEchoServer() throws IOException {
        socket = new DatagramSocket(null);
        socket.setReuseAddress(true);
        socket.bind(new InetSocketAddress(4445));
    }
  
    public static void pr(String text){
    System.out.println(text);
    }
    public void run() {
        running = true;
        pr("Start serwera na porcie 4445");
        while (running) {
            try {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                  String received = new String(packet.getData(), 0, packet.getLength());
                  pr("Otrzymałem paczkę od "+received);
                if (received.equals("end")) {
                    running = false;
                    continue;
                }
                buf  = Engine.getLocalIpAddress().getBytes();
                packet = new DatagramPacket(buf, buf.length, address, port);

                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
                running = false;
            }
        }
        socket.close();
    }
    
      public static void main(String[] args){
           try {
               BroadcastingEchoServer serw= new BroadcastingEchoServer();
                     BroadcastClient client= new BroadcastClient(5);
         
               serw.start();
               client.discoverServers("");
           } catch (IOException ex) {
               Logger.getLogger(BroadcastingEchoServer.class.getName()).log(Level.SEVERE, null, ex);
           } catch (Exception ex) {
            Logger.getLogger(BroadcastingEchoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
}
