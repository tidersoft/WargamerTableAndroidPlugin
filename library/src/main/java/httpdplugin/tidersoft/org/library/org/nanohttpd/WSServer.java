package httpdplugin.tidersoft.org.library.org.nanohttpd;
import com.unity3d.player.UnityPlayer;

import org.json.JSONException;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import httpdplugin.tidersoft.org.library.com.wargamer.server.Engine;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Sender_Game;
import httpdplugin.tidersoft.org.library.org.java_websocket.WebSocket;
import httpdplugin.tidersoft.org.library.org.java_websocket.framing.Framedata;
import httpdplugin.tidersoft.org.library.org.java_websocket.handshake.ClientHandshake;
import httpdplugin.tidersoft.org.library.org.java_websocket.server.WebSocketServer;

/**
 * A simple WebSocketServer implementation. Keeps track of a "chatroom".
 */
public class WSServer extends WebSocketServer {

	ArrayList<WebSocket> browsers;
	ArrayList<WebSocket> boards;
	
	public WSServer( int port ) throws UnknownHostException {
		super( new InetSocketAddress( port ) );
		System.out.println("WebSocket Server start at port: "+port);
	}

	public WSServer( InetSocketAddress address ) {
		super( address );
		}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake ) {
		System.out.println( conn.getLocalSocketAddress().getAddress().getHostAddress() + " entered the room!" );
		//	conn.send("Hello!");
	}

	public boolean isBrowser(WebSocket conn){
		for(int i=0;i<browsers.size();i++){
			if(browsers.get(i).equals(conn))
				return true;

		}
		return false;

	}
	public boolean isBoard(WebSocket conn){
		for(int i=0;i<boards.size();i++){
			if(boards.get(i).equals(conn))
				return true;

		}
		return false;

	}

	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {

		//Engine.instance().CheckPlayers(connections());
		if(isBrowser(conn)) {
			Engine.instance().Logout(Engine.instance().getPlayerNameByWebSock(conn));
			browsers.remove(conn);
		}
		System.out.println( conn + " has left the room!" );
	}

        

      
      
	@Override
	public void onMessage( WebSocket conn, String message ) {

		try {
			System.out.println( conn + ": " + message );
			HashMap<String,String> params= Engine.instance().jsonToMap(message);

			if(params.containsKey("command")){
				if(params.get("command").equals("PostMessage")){
					System.out.println("Posting Message ...");
					Engine.instance().PostMessage(params);
				}
				if(params.get("command").equals("addPlayerBrowser")){
					if(params.containsKey("player")) {
						Engine.instance().addPlayerBrowser(params.get("player"), conn);
						browsers.add(conn);
					}
				}

				if(params.get("command").equals("connectToBoard")){
					if(params.containsKey("name"))
						Engine.instance().ConnectToBoard(params.get("name"),params.get("system"),conn);
						boards.add(conn);
				}
				if(params.get("command").equals("disconnect")){
					if(params.containsKey("name")){
						Engine.instance().DisconnectFromBoard(params.get("name"),conn);

					}

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void onFragment( WebSocket conn, Framedata fragment ) {
		System.out.println( "received fragment: " + fragment );
	}
     
        

	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if( conn != null ) {

		//	Engine.instance().CheckPlayers(connections());
			// some errors like port binding failed may not be assignable to a specific websocket
		}
	}

	
	
	/**
	 * Sends <var>text</var> to all currently connected WebSocket clients.
	 * 
	 * @param text
	 *            The String to send across the network.
	 * @throws InterruptedException
	 *             When socket related I/O errors occur.
	 */
	public void sendToAll( String text ) {
		Collection<WebSocket> con = connections();
		synchronized ( con ) {
			for( WebSocket c : con ) {
				c.send( text );
			}
		}
	}
        
       
}
