package httpdplugin.tidersoft.org.library.org.nanohttpd;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;



	
	public  class TextFile extends ArrayList<String>{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public static String read(String filename)throws IOException{
			
			    BufferedReader dane;
		    	StringBuffer sb = new StringBuffer();
				
		        
	            	
					dane=new BufferedReader(new FileReader(filename));
					String s;
					
					while((s = dane.readLine())!=null){
						sb.append(s);
						sb.append("\n");
					}
					dane.close();
					return sb.toString();
				
				
	          
	            
			
			}
	
		public void write(String filename)throws IOException{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			for(int i=0;i<size();i++)
			out.println(get(i));
			out.close();
			
		}
		public TextFile(String filename)throws IOException{
			
			super(Arrays.asList(read(filename).split("\n")));
			
		}
	
		
		
		
	}
	
	
	

